//***************************************************************
//File: Paint.java
//
//Purpose: Determine how much paint is needed to paint the walls 
//of a room given its length, width, and height
//***************************************************************
import java.util.Scanner;

public class Paint
{
    public static void main(String[] args)
    {
        final int COVERAGE = 350;  //paint covers 350 sq ft/gal
        int length, width, height;
        double totalSqFt;
        double paintNeeded;
        
        Scanner scan = new Scanner(System.in);

        System.out.println("What is the lenght of the room?");
        length = scan.nextInt();
        
        System.out.println("What is the width of the room?");
        width = scan.nextInt();

        System.out.println("What is the height of the room?");
        height = scan.nextInt();

        totalSqFt = (length * width) + (2 * width * height) + (2 * length * height); 
        paintNeeded = totalSqFt / COVERAGE;
        
        System.out.println("A room of lenght " + length + ", width " + width + ", and height "
                             + height + " has a total square footage of " + totalSqFt + 
                           " ft^2 and requires " + paintNeeded + " gallons of paint.");

    }
}