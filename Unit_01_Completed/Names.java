// ***************************************************************
//   Names.java
//
//   Prints a list of student names with their hometowns
//   and intended major
// ***************************************************************

public class Names
{
    // ------------------------
    // main prints the list
    // ------------------------
    public static void main (String[] args)
    {
 System.out.println ();
 System.out.println ("\tName\t\tHometown\tMajor");
 System.out.println ("\t====\t\t========\t=====");
 System.out.println ("\tSally\t\tRoanoke\t\tPhysics");
 System.out.println ("\tAlexander\tWashington\tArts");
 System.out.println ("\tMasta McG\tVancouver\tAwesomeness");
    }
}