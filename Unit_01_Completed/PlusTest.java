// *******************************************************************
//   PlusTest.java
// 
//   Demonstrate the different behaviors of the + operator
// *******************************************************************

public class PlusTest
{
    // -------------------------------------------------
    // main prints some expressions using the + operator
    // -------------------------------------------------
    public static void main (String[] args)
    {
      System.out.println ("This is a long string that is the " +
                          "concatenation of two shorter strings.");

      System.out.println ("The first computer was invented about" + 55 +
                          "years ago.");

      // println starts with a string; therefore it creates a string container.  
      // This causes Java to treat the + sign as concatenation rather than addition.
      System.out.println ("8 plus 5 is " + 8 + 5); 

      // println starts with a string; therefore it creates a string container.  
      // However, because the addition is in brackets, it is done first, then concatenated.       
      System.out.println ("8 plus 5 is " + (8 + 5));

      // println starts with an int; therefore, + is treated like addition.
      // Concatenation is the only operation + can perform with a string.
      System.out.println (8 + 5 + " equals 8 plus 5.");
      
      // Using plus two ways.
      System.out.println ("Ten robins plus " + 1 + 3 + " canaries is " + (10 + 13) + " birds.");
    }
}