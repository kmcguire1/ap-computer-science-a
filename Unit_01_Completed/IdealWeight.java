//***************************************************************
//File: IdealWeight.java
//
//Purpose: Determine how much paint is needed to paint the walls 
//of a room given its length, width, and height
//***************************************************************
import java.util.Scanner;

public class IdealWeight
{
    public static void main(String[] args)
    {
      int weightM, weightF, feet, inches;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.println("How many feet tall are you?");
      feet = scan.nextInt();
        
      System.out.println("How many inches over that foot-height are you?");
      inches = scan.nextInt();

      weightM = 106 + 6 * ((feet - 5) * 12 + inches);
      weightF = 100 + 5 * ((feet - 5) * 12 + inches);
      
      System.out.println("If you are female, your ideal weight range in pounds is " + 
                         (0.85 * weightF) + " to " + (1.15 * weightF));
      System.out.println("If you are male, your ideal weight range in pounds is "  + 
                         (0.85 * weightM) + " to " + (1.15 * weightM));
    }
}