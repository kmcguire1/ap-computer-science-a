// ***************************************************************
//   Grades.java
//
//   Prints a list of student grades on a lab and totals their mark 
// ***************************************************************

public class Grades
{
    public static void main (String[] args)
    {
      System.out.println ();
      System.out.println ("\tName\t\tLab\tBonus\tTotal");
      System.out.println ("\t----\t\t---\t-----\t-----");
      System.out.println ("\tJoe\t\t"+43+"\t"+7+"\t"+(43+7));
      System.out.println ("\tWillian\t\t"+50+"\t"+8+"\t"+(50+8));
      System.out.println ("\tMary Sue\t"+39+"\t"+10+"\t"+(39+10));
      System.out.println ("\tMasta McG\t"+89+"\t"+21+"\t"+(89+21));
    }
}