//**********************************************************
//  Circle.java
//
//  Print the area of a circle with two different radii
//**********************************************************
import java.util.Scanner;

public class Circle
{
    public static void main(String[] args)
    {
     final double PI = 3.14159;
     
     Scanner scan = new Scanner(System.in);
     System.out.println("Enter an integer for the radius for the first circle.");
          
     int radius = scan.nextInt();
     double area1 = PI * radius * radius;
     double circumference1 = 2 * PI * radius;

     System.out.println("The area of a circle with radius " + radius + " is " + area1);
     System.out.println("The circumference of a circle with radius " + radius + " is " + circumference1);

     System.out.println("Enter an integer for the radius for the second circle.");
     radius = scan.nextInt();;
     double area2 = PI * radius * radius;
     double circumference2 = 2 * PI * radius;

     System.out.println("The area of a circle with radius " + radius + " is " + area2);
     System.out.println("The circumference of a circle with radius " + radius + " is " + circumference2);
     
     double areaChange = area2 / area1;
     double circumferenceChange = circumference2 / circumference1;
     
     System.out.println("Changing the radius to " + radius + " increased the area by a factor of " + 
                        areaChange + " and the circumference by a factor of " + circumferenceChange);
    }
}